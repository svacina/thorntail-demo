package demo;

import demo.model.Car;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Collection;


@Path("/car")
@ApplicationScoped
public class CarResource {

    @PersistenceContext(unitName = "AdminPU")
    private EntityManager em;

    // GET ONE

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{carId}")
    public Car get(@PathParam("carId") Integer carId) {
        return em.find(Car.class, carId);
    }

    // GET ALL

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Car> all() throws Exception {
        return em.createNamedQuery("Car.findAll", Car.class)
                .getResultList();
    }

    // CREATE

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response create(Car category) throws Exception {
        try {
            em.persist(category);
            em.flush();
        } catch (ConstraintViolationException cve) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(cve.getMessage())
                    .build();
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }
        return Response
                .created(new URI("car/" + category.getId().toString()))
                .build();
    }

    // UPDATE

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{CarId}")
    @Transactional
    public Response update(@PathParam("CarId") Integer CarId, Car Car) throws Exception {
        try {
            Car entity = em.find(Car.class, CarId);

            if (null == entity) {
                return Response
                        .status(Response.Status.NOT_FOUND)
                        .entity("Category with id of " + CarId + " does not exist.")
                        .build();
            }

            em.merge(Car);

            return Response
                    .ok(Car)
                    .build();
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }
    }

    // REMOVE

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{CarId}")
    @Transactional
    public Response remove(@PathParam("CarId") Integer CarId) throws Exception {
        try {
            Car entity = em.find(Car.class, CarId);
            em.remove(entity);
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }

        return Response
                .noContent()
                .build();
    }

}