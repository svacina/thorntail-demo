package demo.model;

/**
 * @author Ken Finnigan
 */
public class TestCarObject extends Car {
    public TestCarObject(Integer id) {
        this.id = id;
    }

    public TestCarObject(Integer id,
                  String brand,
                  String type,
                  String licencePlate,
                         Person owner) {
        this.id = id;
        this.brand = brand;
        this.type = type;
        this.licencePlate = licencePlate;
        this.owner = owner;
    }
}
