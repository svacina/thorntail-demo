package demo.model;

public class TestPersonObject extends Person {
    public TestPersonObject(Integer id) {
        this.id = id;
    }

    public TestPersonObject(Integer id,
                         String name,
                         String email,
                         Integer age) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.age = age;
    }
}
