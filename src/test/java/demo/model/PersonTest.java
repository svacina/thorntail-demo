package demo.model;

import org.fest.assertions.Assertions;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;


import demo.model.Person;
import demo.model.TestPersonObject;
import org.fest.assertions.Assertions;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Ken Finnigan
 */
public class PersonTest {

    @Test
    public void personsAreEqual() throws Exception {
        Person person1 = createPerson(1, "gen. Václav Morávek", "moravek@acr.cz", 96);
        Person person2 = createPerson(1, "gen. Václav Morávek", "moravek@acr.cz", 96);

        Assertions.assertThat(person1).isEqualTo(person2);
        assertThat(person1.equals(person2)).isTrue();
        assertThat(person1.hashCode()).isEqualTo(person2.hashCode());
    }

    @Test
    public void personsAreNotEqual() throws Exception {
        Person person1 = createPerson(2, "gen. Václav Morávek", "moravek@acr.cz", 96);
        Person person2 = createPerson(1, "gen. Miloslav Masopust", "masopust@acr.cz", 98);

        Assertions.assertThat(person1).isNotEqualTo(person2);
        assertThat(person1.equals(person2)).isFalse();
        assertThat(person1.hashCode()).isNotEqualTo(person2.hashCode());
    }

    @Test
    public void PersonModification() throws Exception {
        Person person1 = createPerson(1, "gen. Václav Morávek", "moravek@acr.cz", 96);
        Person person2 = createPerson(1, "gen. Václav Morávek", "moravek@acr.cz", 96);

        Assertions.assertThat(person1).isEqualTo(person2);
        assertThat(person1.equals(person2)).isTrue();
        assertThat(person1.hashCode()).isEqualTo(person2.hashCode());

        person1.setEmail("moravek.vaclav@acr.cz");

        Assertions.assertThat(person1).isNotEqualTo(person2);
        assertThat(person1.equals(person2)).isFalse();
        assertThat(person1.hashCode()).isNotEqualTo(person2.hashCode());
    }


    private Person createPerson(Integer id, String name, String email, Integer age) {
        return new TestPersonObject(id, name, email, age);
    }
}