package demo;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import demo.model.Car;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.wildfly.swarm.arquillian.DefaultDeployment;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.awaitility.Awaitility.await;
import static org.fest.assertions.Assertions.assertThat;



//import org.arquillian.cube.openshift.impl.enricher.RouteURL;


@RunWith(Arquillian.class)
//@DefaultDeployment
//@RunAsClient
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EndpointResourceIT {

//    @RouteURL("demo")
//    private URL url;
//
//    @Before
//    public void verifyRunning() {
//        await()
//                .atMost(1, TimeUnit.MINUTES)
//                .until(() -> {
//                    try {
//                        return get(url + "hello").statusCode() == 200;
//                    } catch (Exception e) {
//                        return false;
//                    }
//                });
//
//        RestAssured.baseURI = url + "";
//    }

    @BeforeClass
    public static void setup() throws Exception {
        RestAssured.baseURI = "http://localhost:8080";
    }

    @Test
    public void testHello() throws Exception {

        Response response =
                when()
                        .get("/hello")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString).isEqualTo("Hello from Thorntail!");
    }

    @Test
    public void testGetCar() throws Exception {
        Response response =
//                given()
//                        .pathParam("carId", 100)
                when()
                        .get("/car/list")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();
//        Car car = JsonPath.from(jsonAsString).getObject("", Car.class);

//        (100, 'Volvo', 'XC90', 'ZL 96-98');

//        assertThat(car.getId()).isEqualTo(100);
    }
}
