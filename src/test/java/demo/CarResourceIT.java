package demo;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import static io.restassured.RestAssured.when;
import static org.fest.assertions.Assertions.assertThat;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import demo.model.Car;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.wildfly.swarm.arquillian.DefaultDeployment;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.awaitility.Awaitility.await;
import static org.fest.assertions.Assertions.assertThat;


// ToDo: Uncomment for minishift
//import org.jboss.arquillian.container.test.api.RunAsClient;


// ToDo: Uncomment for minishift

//import org.arquillian.cube.openshift.impl.enricher.RouteURL;

// ToDo: Uncomment for minishift
@RunWith(Arquillian.class)
//@DefaultDeployment
//@RunAsClient
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CarResourceIT {

    // ToDo: Uncomment for minishift

//    @RouteURL("demo")
//    private URL url;
//
//    @Before
//    public void verifyRunning() {
//        await()
//                .atMost(1, TimeUnit.MINUTES)
//                .until(() -> {
//                    try {
//                        return get(url + "hello").statusCode() == 200;
//                    } catch (Exception e) {
//                        return false;
//                    }
//                });
//
//        RestAssured.baseURI = url + "";
//    }

    @BeforeClass
    public static void setup() throws Exception {
        RestAssured.baseURI = "http://localhost:8080";
    }

    @Test
    public void aTestHello() throws Exception {

        Response response =
                when()
                        .get("/hello")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString).isEqualTo("Hello from Thorntail!");
    }

    @Test
    public void bTestGetCar() throws Exception {
        Response response =
                given()
                        .pathParam("carId", 100)
                .when()
                        .get("/car/{carId}")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();
        Car record1 = JsonPath.from(jsonAsString).getObject("", Car.class);

        assertThat(record1.getId()).isEqualTo(100);
        assertThat(record1.getBrand()).isEqualTo("Volvo");
        assertThat(record1.getType()).isEqualTo("XC90");
        assertThat(record1.getLicencePlate()).isEqualTo("ZL 96-98");
        assertThat(record1.getOwner().getId()).isEqualTo(1);
    }

    @Test
    public void cTestGetAll() throws Exception {
        Response response =
                when()
                        .get("/car/list")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();
        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");

        assertThat(jsonAsList.size()).isEqualTo(2);

        Map<String, ?> record1 = jsonAsList.get(0);

        assertThat(record1.get("id")).isEqualTo(100);
        assertThat(record1.get("brand")).isEqualTo("Volvo");
        assertThat(record1.get("type")).isEqualTo("XC90");
        assertThat(record1.get("licencePlate")).isEqualTo("ZL 96-98");

        Map<String, ?> record2 = jsonAsList.get(1);

        assertThat(record2.get("id")).isEqualTo(101);
        assertThat(record2.get("brand")).isEqualTo("Volvo");
        assertThat(record2.get("type")).isEqualTo("XC60");
        assertThat(record2.get("licencePlate")).isEqualTo("ZL 95-97");
    }

    @Test
    public void dCreate() throws Exception {
        Car skoda = new Car();
        skoda.setBrand("Skoda");
        skoda.setType("120L");
        skoda.setLicencePlate("UH 12-47");

        // CREATE
        Response response =
                given()
                        .contentType(ContentType.JSON)
                        .body(skoda)
                .when()
                        .post("/car");
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(201);
        String locationUrl = response.getHeader("Location");
        Integer carId = Integer.valueOf(locationUrl.substring(locationUrl.lastIndexOf('/') + 1));

        // GET
        response =
                given()
                        .pathParam("carId", carId)
                .when()
                        .get("/car/{carId}")
                .then()
                        .extract().response();
        String jsonAsString = response.asString();
        Car car = JsonPath.from(jsonAsString).getObject("", Car.class);
        assertThat(car.getId()).isEqualTo(carId);
        assertThat(car.getBrand()).isEqualTo("Skoda");
        assertThat(car.getType()).isEqualTo("120L");
        assertThat(car.getLicencePlate()).isEqualTo("UH 12-47");

        // DELETE
        response =
                given()
                        .contentType(ContentType.JSON)
                        .pathParam("carId", car.getId())
                        .when()
                        .delete("/car/{carId}");

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(204);
    }

    @Test
    public void eDeleteCar() throws Exception {
        Car skoda = new Car();
        skoda.setBrand("Volvo");
        skoda.setType("V60");
        skoda.setLicencePlate("KM 35-47");

        // CREATE
        Response response =
                given()
                        .contentType(ContentType.JSON)
                        .body(skoda)
                        .when()
                        .post("/car");
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(201);
        String locationUrl = response.getHeader("Location");
        Integer carId = Integer.valueOf(locationUrl.substring(locationUrl.lastIndexOf('/') + 1));

        //DELETE
        response =
                given()
                        .contentType(ContentType.JSON)
                        .pathParam("carId", carId)
                        .when()
                        .delete("/car/{carId}");

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(204);


        //GET
        Response responseGetAfterDelete =
                given()
                        .pathParam("carId", carId)
                        .when()
                        .get("/car/{carId}")
                        .then()
                        .extract().response();



        assertThat(responseGetAfterDelete).isNotNull();
        assertThat(responseGetAfterDelete.getStatusCode()).isEqualTo(204);

    }

    @Test
    public void fUpdateCar() throws Exception {
        Car car = new Car();
        car.setBrand("Skoda");
        car.setType("100K");
        car.setLicencePlate("VS 45-11");

        // CREATE
        Response response =
                given()
                        .contentType(ContentType.JSON)
                        .body(car)
                        .when()
                        .post("/car");
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(201);
        String locationUrl = response.getHeader("Location");
        Integer carId = Integer.valueOf(locationUrl.substring(locationUrl.lastIndexOf('/') + 1));

        // UPDATE
        car.setId(carId);
        car.setBrand("Volvo");
        car.setType("V80");
        car.setLicencePlate("ZL 45-11");
        response =
                given()
                        .contentType(ContentType.JSON)
                        .pathParam("carId", carId)
                        .body(car)
                        .when()
                        .put("/car/{carId}");
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(200);
        // GET
        response =
                given()
                        .pathParam("carId", carId)
                        .when()
                        .get("/car/{carId}")
                        .then()
                        .extract().response();
        String jsonAsString = response.asString();
        car = JsonPath.from(jsonAsString).getObject("", Car.class);
        assertThat(car.getId()).isEqualTo(carId);
        assertThat(car.getBrand()).isEqualTo("Volvo");
        assertThat(car.getType()).isEqualTo("V80");
        assertThat(car.getLicencePlate()).isEqualTo("ZL 45-11");

        // DELETE
        response =
                given()
                        .contentType(ContentType.JSON)
                        .pathParam("carId", carId)
                        .when()
                        .delete("/car/{carId}");

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(204);


        //GET
        Response responseGetAfterDelete =
                given()
                        .pathParam("carId", carId)
                        .when()
                        .get("/car/{carId}")
                        .then()
                        .extract().response();



        assertThat(responseGetAfterDelete).isNotNull();
        assertThat(responseGetAfterDelete.getStatusCode()).isEqualTo(204);

    }





}