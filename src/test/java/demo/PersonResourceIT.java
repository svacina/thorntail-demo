package demo;




import demo.model.Person;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import static io.restassured.RestAssured.when;
import static org.fest.assertions.Assertions.assertThat;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import demo.model.Car;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.wildfly.swarm.arquillian.DefaultDeployment;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.awaitility.Awaitility.await;
import static org.fest.assertions.Assertions.assertThat;

// ToDo: Uncomment for minishift

//import org.arquillian.cube.openshift.impl.enricher.RouteURL;

// ToDo: Uncomment for minishift
@RunWith(Arquillian.class)
//@DefaultDeployment
//@RunAsClient
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PersonResourceIT {

    // ToDo: Uncomment for minishift
//
//    @RouteURL("demo")
//    private URL url;
//
//    @Before
//    public void verifyRunning() {
//        await()
//                .atMost(1, TimeUnit.MINUTES)
//                .until(() -> {
//                    try {
//                        return get(url + "hello").statusCode() == 200;
//                    } catch (Exception e) {
//                        return false;
//                    }
//                });
//
//        RestAssured.baseURI = url + "";
//    }

    @BeforeClass
    public static void setup() throws Exception {
        RestAssured.baseURI = "http://localhost:8080";
    }

    @Test
    public void aTestHello() throws Exception {

        Response response =
                when()
                        .get("/hello")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();
        assertThat(jsonAsString).isEqualTo("Hello from Thorntail!");
    }

    @Test
    public void bGetPerson() throws Exception {
        Response response =
                given()
                        .pathParam("personId", 1)
                        .when()
                        .get("/person/{personId}")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();
        Person record1 = JsonPath.from(jsonAsString).getObject("", Person.class);

        assertThat(record1.getId()).isEqualTo(1);
        assertThat(record1.getName()).isEqualTo("gen. Josef Balaban");
        assertThat(record1.getEmail()).isEqualTo("balaban@acr.cz");
        assertThat(record1.getAge()).isEqualTo(98);
    }

    @Test
    public void cGetAll() throws Exception {
        Response response =
                when()
                        .get("/person/all")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();
        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");

        assertThat(jsonAsList.size()).isEqualTo(2);

        Map<String, ?> record1 = jsonAsList.get(0);

        assertThat(record1.get("id")).isEqualTo(1);
        assertThat(record1.get("name")).isEqualTo("gen. Josef Balaban");
        assertThat(record1.get("email")).isEqualTo("balaban@acr.cz");
        assertThat(record1.get("age")).isEqualTo(98);

        Map<String, ?> record2 = jsonAsList.get(1);

        assertThat(record2.get("id")).isEqualTo(2);
        assertThat(record2.get("name")).isEqualTo("gen. Emil Strankmüller");
        assertThat(record2.get("email")).isEqualTo("strankmuller@acr.cz");
        assertThat(record2.get("age")).isEqualTo(92);
    }

    @Test
    public void dCreate() throws Exception {
        Person person = new Person();
        person.setName("gen. Václav Morávek");
        person.setEmail("moravek@acr.cz");
        person.setAge(96);

        // CREATE
        Response response =
                given()
                        .contentType(ContentType.JSON)
                        .body(person)
                        .when()
                        .post("/person");
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(201);
        String locationUrl = response.getHeader("Location");
        Integer personId = Integer.valueOf(locationUrl.substring(locationUrl.lastIndexOf('/') + 1));

        // GET
        response =
                given()
                        .pathParam("personId", personId)
                        .when()
                        .get("/person/{personId}")
                        .then()
                        .extract().response();
        String jsonAsString = response.asString();
        person = JsonPath.from(jsonAsString).getObject("", Person.class);
        assertThat(person.getId()).isEqualTo(personId);
        assertThat(person.getName()).isEqualTo("gen. Václav Morávek");
        assertThat(person.getEmail()).isEqualTo("moravek@acr.cz");
        assertThat(person.getAge()).isEqualTo(96);

        // DELETE
        response =
                given()
                        .contentType(ContentType.JSON)
                        .pathParam("personId", person.getId())
                        .when()
                        .delete("/person/{personId}");

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(204);
    }

    @Test
    public void eDeletePerson() throws Exception {
        Person person = new Person();
        person.setName("gen. Miloslav Masopust");
        person.setEmail("moravek@acr.cz");
        person.setAge(89);

        // CREATE
        Response response =
                given()
                        .contentType(ContentType.JSON)
                        .body(person)
                        .when()
                        .post("/person");
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(201);
        String locationUrl = response.getHeader("Location");
        Integer personId = Integer.valueOf(locationUrl.substring(locationUrl.lastIndexOf('/') + 1));

        //DELETE
        response =
                given()
                        .contentType(ContentType.JSON)
                        .pathParam("personId", personId)
                        .when()
                        .delete("/person/{personId}");

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(204);


        //GET
        Response responseGetAfterDelete =
                given()
                        .pathParam("personId", personId)
                        .when()
                        .get("/person/{personId}")
                        .then()
                        .extract().response();



        assertThat(responseGetAfterDelete).isNotNull();
        assertThat(responseGetAfterDelete.getStatusCode()).isEqualTo(204);
    }


    @Test
    public void fUpdatePerson() throws Exception {
        Person person = new Person();
        person.setName("gen. Miloslav Masopust");
        person.setEmail("moravek@acr.cz");
        person.setAge(89);

        // CREATE
        Response response =
                given()
                        .contentType(ContentType.JSON)
                        .body(person)
                        .when()
                        .post("/person");
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(201);
        String locationUrl = response.getHeader("Location");
        Integer personId = Integer.valueOf(locationUrl.substring(locationUrl.lastIndexOf('/') + 1));

        // UPDATE
        person.setId(personId);
        person.setName("gen. Josef Mašín");
        person.setEmail("masin@acr.cz");
        person.setAge(91);
        response =
                given()
                        .contentType(ContentType.JSON)
                        .pathParam("personId", personId)
                        .body(person)
                        .when()
                        .put("/person/{personId}");
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(200);
        // GET
        response =
                given()
                        .pathParam("personId", personId)
                        .when()
                        .get("/person/{personId}")
                        .then()
                        .extract().response();
        String jsonAsString = response.asString();
        person = JsonPath.from(jsonAsString).getObject("", Person.class);
        assertThat(person.getId()).isEqualTo(personId);
        assertThat(person.getName()).isEqualTo("gen. Josef Mašín");
        assertThat(person.getEmail()).isEqualTo("masin@acr.cz");
        assertThat(person.getAge()).isEqualTo(91);

        // DELETE
        response =
                given()
                        .contentType(ContentType.JSON)
                        .pathParam("personId", personId)
                        .when()
                        .delete("/person/{personId}");

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(204);


        //GET
        Response responseGetAfterDelete =
                given()
                        .pathParam("personId", personId)
                        .when()
                        .get("/person/{personId}")
                        .then()
                        .extract().response();

        assertThat(responseGetAfterDelete).isNotNull();
        assertThat(responseGetAfterDelete.getStatusCode()).isEqualTo(204);

    }



}